﻿using System;
using System.Collections.Generic;
using System.Text;
using TP3.Models;

namespace TP3.Services
{
    public class PizzaService
    {
        private List<Pizza> _pizzas;

        // Design Pattern Singleton
        private PizzaService() { } // Constructeur privé
        private static PizzaService _Instance; // Stockage de l'instance unique de la classe
        public static PizzaService Instance
        {
            get
            {
                if (_Instance == null)
                {
                    _Instance = new PizzaService
                    {
                        _pizzas = new List<Pizza>
                        {
                            new Pizza { Nom = "Margarita", EstVegetarienne = true, Ingredients = "Tomate, Fromage", Prix = 10 },
                            new Pizza { Nom = "Royale", EstVegetarienne = false, Ingredients = "Tomate, Fromage, Jambon", Prix = 11.8 },
                            new Pizza { Nom = "Reine", EstVegetarienne = false, Ingredients = "Tomate, Fromage, Jambon, Oeuf", Prix = 12.5 }
                        }
                    };
                }

                return _Instance;
            }
        }

        public List<Pizza> GetPizzas()
        {
            return _pizzas;
        }
    }
}
