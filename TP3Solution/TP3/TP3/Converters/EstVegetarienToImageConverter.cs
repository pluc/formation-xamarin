﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using Xamarin.Forms;

namespace TP3.Converters
{
    public class EstVegetarienToImageConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            bool? estVegetarien = (bool?)value;

            if (estVegetarien == null)
            {
                estVegetarien = false;
            }

            string imageName = "carnivore.jpg";

            if (estVegetarien.Value)
            {
                imageName = "vegetarien.jpg";
            }

            return ImageSource.FromResource($"TP3.Resources.{imageName}", typeof(EstVegetarienToImageConverter).Assembly);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
