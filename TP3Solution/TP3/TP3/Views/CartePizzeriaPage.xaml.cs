﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TP3.Models;
using TP3.Services;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TP3.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CartePizzeriaPage : ContentPage
    {
        public CartePizzeriaPage()
        {
            InitializeComponent();

            List<Pizza> pizzas = PizzaService.Instance.GetPizzas();

            this.lstPizzas.ItemsSource = pizzas;
        }

        private void lstPizzas_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            Pizza pizza = e.Item as Pizza;

            this.Navigation.PushAsync(new DetailPizzaPage(pizza));
        }
    }
}