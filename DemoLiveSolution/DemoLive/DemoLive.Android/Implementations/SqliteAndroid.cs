﻿using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using DemoLive.Droid.Implementations;
using DemoLive.Interfaces;
using SQLite;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

[assembly: Xamarin.Forms.Dependency(typeof(SqliteAndroid))]
namespace DemoLive.Droid.Implementations
{
    public class SqliteAndroid : ISqlite
    {
        public SQLiteConnection GetConnection(string dbName)
        {
            string folderPath = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);

            string dbFullPath = Path.Combine(folderPath, dbName);

            return new SQLiteConnection(dbFullPath);
        }
    }
}