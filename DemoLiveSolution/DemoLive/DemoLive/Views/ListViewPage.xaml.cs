﻿using DemoLive.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace DemoLive.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ListViewPage : ContentPage
    {
        public ListViewPage()
        {
            InitializeComponent();

            this.myList1.ItemsSource = new string[] { "Toto", "Titi", "Tata", "Tutu" };
            this.myList2.ItemsSource = new User[]
            {
                new User { Name = "Toto" },
                new User { Name = "Titi" },
                new User { Name = "Tata" }
            };
        }
    }
}