﻿using DemoLive.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace DemoLive.ViewModels
{
    public class GuitareViewModel : BaseViewModel
    {
        public string Nom { get; set; }

        private int _NombreCordes;
        public int NombreCordes
        {
            get
            {
                return this._NombreCordes;
            }
            set
            {
                if (this.Set(value, ref this._NombreCordes))
                {
                    // Si le nombre de corde a changé (on est là)
                    this.CmdIncrementerCordes.ChangeCanExecute();
                }
            }
        }

        public Command CmdIncrementerCordes { get; set; }
        public Command CmdResetNombreCordes { get; set; }

        public GuitareViewModel()
        {
            // () => this.IncrementerCordes() => C'est une expression lambda, point IMPORTANT À CONNAITRE en C# (et dans plusieurs autres langages)
            // this.CmdIncrementerCordes = new Command(() => this.IncrementerCordes(), () => this.CanIncrementerCordes());

            this.CmdIncrementerCordes = new Command(this.IncrementerCordes, this.CanIncrementerCordes);
            this.CmdResetNombreCordes = new Command(this.ResetNombreCordes);
        }

        private void ResetNombreCordes()
        {
            this.NombreCordes = 0;

            IShowMessage service = Xamarin.Forms.DependencyService.Get<IShowMessage>();

            service.ShowMessage("Nombre de cordes réinitialisé.");
        }

        private void IncrementerCordes()
        {
            this.NombreCordes++;
        }

        private bool CanIncrementerCordes()
        {
            return this.NombreCordes < 12;
        }
    }
}
