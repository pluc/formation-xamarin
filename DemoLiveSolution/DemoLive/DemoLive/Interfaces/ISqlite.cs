﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DemoLive.Interfaces
{
    public interface ISqlite
    {
        SQLite.SQLiteConnection GetConnection(string dbName);
    }
}
