﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DemoLive.Interfaces
{
    public interface IShowMessage
    {
        void ShowMessage(string message);
    }
}
