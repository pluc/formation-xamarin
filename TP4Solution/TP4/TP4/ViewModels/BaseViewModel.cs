﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;

namespace TP4.ViewModels
{
    public class BaseViewModel : INotifyPropertyChanged
    {
        /// <summary>
        /// Implémentation de l'interface INotifyPropertyChanged
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        protected virtual bool Set<T>(T value, ref T field, [CallerMemberName] string propertyName = null)
        {
            bool areEquals = EqualityComparer<T>.Default.Equals(value, field);

            if (!areEquals)
            {
                field = value;

                this.OnPropertyChanged(propertyName);
            }

            return !areEquals;
        }
    }
}
