﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TP4.Extensions
{
    public static class EnumerableExtension
    {
        public static IEnumerable<T> RandomSort<T>(this IEnumerable<T> enumerable)
        {
            return enumerable.OrderBy(e => Guid.NewGuid());
        }
    }
}
