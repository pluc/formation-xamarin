﻿using System;
using System.Collections.Generic;
using System.Text;
using TP4.Models;

namespace TP4.Services
{
    public class UserBankService
    {
        private UserBankService() { }
        private static UserBankService _Instance;
        public static UserBankService Instance
        {
            get
            {
                if (_Instance == null)
                {
                    _Instance = new UserBankService();
                }

                return _Instance;
            }
        }

        private UserBank _validUserBank => new UserBank { AccountNumber = 1234567890, Password = "1234", Email = "user@bank", FirstName = "User", LastName = "Bank" };
        private UserBank _currentUserBank;

        public bool Authenticate(string accountNumber, string password)
        {
            bool authSuccess = true;

            if (accountNumber != this._validUserBank.AccountNumber.ToString() || password != this._validUserBank.Password)
            {
                authSuccess = false;
                this._currentUserBank = null;
            }
            else
            {
                this._currentUserBank = this._validUserBank;
            }

            return authSuccess;
        }

        public bool IsAuthenticated => this._currentUserBank != null;

        public void Logout()
        {
            this._currentUserBank = null;
            
            App.Current.MainPage.Navigation.PopToRootAsync();
        }

        public UserBank GetCurrentUser()
        {
            return this._currentUserBank;
        }
    }
}
