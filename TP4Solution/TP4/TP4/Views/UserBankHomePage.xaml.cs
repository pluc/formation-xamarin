﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TP4.Services;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TP4.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class UserBankHomePage : ContentPage
    {
        public UserBankHomePage()
        {
            InitializeComponent();

            // Si le user n'est pas authentifié
            if (!UserBankService.Instance.IsAuthenticated)
            {
                // Redirection de l'utilisateur sur la page d'accueil de l'application
                Application.Current.MainPage.Navigation.PopToRootAsync();
            }
        }
    }
}